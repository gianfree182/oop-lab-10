package it.unibo.oop.lab.reactivegui01;

import static org.junit.Assert.assertTrue;

/**
 * TestMatrix class for first reactive GUI.
 *
 */
public final class Test {

    private Test() {


    }

   /**
     * Analyze the behavior of the CGUI.java file executing this test
     * 
     * @param args
     *            possible command line arguments (not used)
     * 
     */
    public static void main(final String... args) {
        new ConcurrentGUI();

    }
}
