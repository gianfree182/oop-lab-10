package it.unibo.oop.lab.reactivegui03;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * 
 * New feature for the first example
 *
 */
public class AnotherConcurrentGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private final static float SIZEFONT = 50;
    private final JFrame frame = new JFrame();
    private final JPanel panel = new JPanel();
    private final JButton buttonUp;
    private final JButton buttonDown;
    private final JButton buttonStop;
    private final JLabel text;
    private final Agent agent;
    private final AgentWait agentWait;

    /**
     * 
     * 
     * GUI with three button and one label.
     */
    public AnotherConcurrentGUI() {

        buttonUp = new JButton("up");
        buttonUp.setFont(buttonUp.getFont().deriveFont(SIZEFONT));
        buttonDown = new JButton("down");
        buttonDown.setFont(buttonUp.getFont().deriveFont(SIZEFONT));
        buttonStop = new JButton("stop");
        buttonStop.setFont(buttonUp.getFont().deriveFont(SIZEFONT));
        text = new JLabel();
        text.setFont(text.getFont().deriveFont(SIZEFONT));

        frame.add(panel);

        panel.setLayout(new FlowLayout());
        panel.add(text);
        panel.add(buttonUp);
        panel.add(buttonDown);
        panel.add(buttonStop);

        frame.setVisible(true);
        frame.pack();

        agent = new Agent();
        new Thread(agent).start();

        agentWait = new AgentWait();
        new Thread(agentWait).start();

        buttonDown.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                if (!agent.isInverse()) {
                    agent.inverseCounting();
                }

            }

        });

        buttonUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent a) {
                if (agent.isInverse()) {
                    agent.notInverseCounting();
                }
            }
        });

        buttonStop.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                agent.stopCounting();
                buttonUp.setEnabled(false);
                buttonDown.setEnabled(false);
            }

        });

    }

    private class Agent implements Runnable {

        private volatile boolean stop;
        private boolean inverse;
        private int counter;

        public void run() {

            while (!stop) {

                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            text.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                } catch (InvocationTargetException | InterruptedException e1) {
                    e1.printStackTrace();
                }
                if (!inverse) {
                    this.counter++;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    this.counter--;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }

        /**
         * 
         * 
         * @return
         */
        public boolean isInverse() {
            return this.inverse;
        }

        /**
         *
         */
        public void inverseCounting() {
            this.inverse = true;
        }

        /**
         * 
         * 
         */
        public void notInverseCounting() {
            this.inverse = false;
        }

        /**
         * 
         * 
         */
        public void stopCounting() {
            this.stop = true;
        }

    }

    private class AgentWait implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(10000);
                agent.stopCounting();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

}
